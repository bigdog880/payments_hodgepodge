package com.tomorrow.payments_hodgepodge.service;

import com.tomorrow.payments_hodgepodge.util.CommonResult;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Tomorrow
 * @date 2020/11/02 23:58
 */
public interface PayPalService {

    /**
     * 创建订单
     *
     * @param amount
     * @return
     */
    CommonResult createOrder(String amount);

    /**
     * 捕获订单
     *
     * @param token
     * @return
     */
    CommonResult captureOrder(String token);

    /**
     * 查询订单
     *
     * @param token
     * @return
     */
    CommonResult queryOrder(String token);

    /**
     * 申请退款
     *
     * @param token
     * @return
     */
    CommonResult refundOrder(String token);

    /**
     * 支付成功回调通知
     *
     * @param request
     * @return
     */
    CommonResult paypalSuccess(HttpServletRequest request);

    /**
     * 支付失败回调通知
     *
     * @param request
     * @return
     */
    CommonResult paypalFail(HttpServletRequest request);
}
