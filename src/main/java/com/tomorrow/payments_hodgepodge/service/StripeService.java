package com.tomorrow.payments_hodgepodge.service;

import com.tomorrow.payments_hodgepodge.util.CommonResult;

/**
 * @author Tomorrow
 * @date 2020/11/13 14:57
 */
public interface StripeService {
    /**
     * 创建 SetupIntent
     *
     * @return
     */
    CommonResult createSetupIntent();

    /**
     * 获取银行卡信息
     *
     * @param customerId 创建 SetupIntent 返回的 customerId
     * @return
     */
    CommonResult getBankCardInformation(String customerId);

    /**
     * 支付
     *
     * @param paymentMethodId
     * @param customerId
     * @return
     */
    CommonResult payOrder(String paymentMethodId, String customerId);


    /* --------------------------------------------------------------------- **/
    /* --------------------------------------------------------------------- **/

    /**
     * stripe 授权
     *
     * @param orderNo 订单号
     * @return
     */
    CommonResult stripeAuthorize(String orderNo);

    /**
     * stripe 捕获
     *
     * @param token
     * @return
     */
    CommonResult stripeCapture(String token);
}
