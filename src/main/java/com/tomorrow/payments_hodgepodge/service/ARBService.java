package com.tomorrow.payments_hodgepodge.service;

import com.tomorrow.payments_hodgepodge.util.CommonResult;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Tomorrow
 * @date 2020/11/3 17:41
 */
public interface ARBService {

    /**
     * 创建订单
     *
     * @param orderNo 订单号
     * @return
     */
    CommonResult createOrder(String orderNo);

    /**
     * 回调地址
     *
     * @param request
     * @return
     */
    CommonResult callback(HttpServletRequest request);

    /**
     * ARB 退款
     *
     * @param amount        退款金额
     * @param refundOrderNo 退款transId支付成功后返回的数据
     * @return
     */
    CommonResult arbRefund(String amount, String refundOrderNo);
}
