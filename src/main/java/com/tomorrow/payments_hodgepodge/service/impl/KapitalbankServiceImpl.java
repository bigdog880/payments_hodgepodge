package com.tomorrow.payments_hodgepodge.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.tomorrow.payments_hodgepodge.service.KapitalbankService;
import com.tomorrow.payments_hodgepodge.util.CommonResult;
import com.tomorrow.payments_hodgepodge.util.HttpsUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Tomorrow
 * @date 2020/11/17 20:07
 */
@Service
@Slf4j
public class KapitalbankServiceImpl implements KapitalbankService {
    @Value("${project.KAPITAL_BANK.TOKEN}")
    public String TOKEN;
    @Value("${project.KAPITAL_BANK.CASH_ID}")
    public String CASH_ID;

    @Value("${project.KAPITAL_BANK.GET_TOKEN}")
    public String GET_TOKEN;
    @Value("${project.KAPITAL_BANK.CREATE_ORDER}")
    public String CREATE_ORDER;
    @Value("${project.KAPITAL_BANK.PAY_ORDER}")
    public String PAY_ORDER;
    @Value("${project.KAPITAL_BANK.QUERY_ORDER}")
    public String QUERY_ORDER;

    @Override
    public CommonResult kapitalbankTiedCard(String cardNumber, String expYearMonth) {
        Map<String, String> map = new HashMap<>();
        map.put("content-type", "application/json");
        map.put("Authorization", TOKEN);

        try {
            String getToken = GET_TOKEN.replaceAll("PAN", cardNumber.replace(" ", "")).replaceAll("EXPIRY", expYearMonth.replace("/", ""));
            String post = HttpsUtils.HttpsRequest(getToken, "GET", null, map);

            JSONObject jsonObject = JSONObject.parseObject(post);
            if (!"OK".equals(jsonObject.getString("status"))) {
                return CommonResult.failMessage(500, "FAIL", jsonObject.getString("errorMessage"));
            }
            // 获取 token
            String data = jsonObject.getString("data");
            JSONObject jsonObjectData = JSONObject.parseObject(data);
            return CommonResult.success("SUCCESS", jsonObjectData.getString("token"));
        } catch (Exception e) {
            log.error("Kapitalbank 绑卡操作异常: " + e);
            return CommonResult.fail(500, "NetworkTimeout");
        }
    }

    @Override
    public CommonResult createKapitalbank(int amount, String token) {
        Map<String, String> map = new HashMap<>();
        map.put("content-type", "application/json");
        map.put("Authorization", TOKEN);

        JSONObject jsonObjectRequest = new JSONObject();
        jsonObjectRequest.put("amount", amount);
        jsonObjectRequest.put("cashId", CASH_ID);
        jsonObjectRequest.put("token", token);
        jsonObjectRequest.put("userData", "jsonObject");
        try {
            String post = HttpsUtils.HttpsRequest(CREATE_ORDER, "POST", jsonObjectRequest.toString(), map);

            JSONObject jsonObjectResponse = JSONObject.parseObject(post);
            if (!"OK".equals(jsonObjectResponse.getString("status"))) {
                return CommonResult.failMessage(500, "FAIL", jsonObjectResponse.getString("errorMessage"));
            }

            // 获取 data
            String data = jsonObjectResponse.getString("data");
            return CommonResult.success("SUCCESS", data);
        } catch (Exception e) {
            log.error("Kapitalbank 创建交易操作异常: " + e);
            return CommonResult.fail(500, "NetworkTimeout");
        }
    }

    @Override
    public CommonResult payKapitalbank(String smsCode, String transactionId) {
        Map<String, String> map = new HashMap<>();
        map.put("content-type", "application/json");
        map.put("Authorization", TOKEN);

        JSONObject jsonObjectRequest = new JSONObject();
        jsonObjectRequest.put("cashId", CASH_ID);
        jsonObjectRequest.put("sms", smsCode);
        jsonObjectRequest.put("transactionId", transactionId);
        try {
            String post = HttpsUtils.HttpsRequest(PAY_ORDER, "POST", jsonObjectRequest.toString(), map);

            JSONObject jsonObjectResponse = JSONObject.parseObject(post);
            if (!"OK".equals(jsonObjectResponse.getString("status")) || !"success".equals(jsonObjectResponse.getString("data"))) {
                return CommonResult.failMessage(500, "FAIL", jsonObjectResponse.getString("errorMessage"));
            }
            return CommonResult.success("SUCCESS");
        } catch (Exception e) {
            log.error("Kapitalbank 交易付款操作异常: " + e);
            return CommonResult.fail(500, "NetworkTimeout");
        }
    }

    @Override
    public CommonResult quyerKapitalbank(String transactionId) {
        Map<String, String> map = new HashMap<>();
        map.put("content-type", "application/json");
        map.put("Authorization", TOKEN);

        JSONObject jsonObjectRequest = new JSONObject();
        jsonObjectRequest.put("cashId", CASH_ID);
        jsonObjectRequest.put("transactionId", transactionId);
        try {
            String post = HttpsUtils.HttpsRequest(QUERY_ORDER, "POST", jsonObjectRequest.toString(), map);

            JSONObject jsonObjectResponse = JSONObject.parseObject(post);
            if (!"OK".equals(jsonObjectResponse.getString("status"))) {
                return CommonResult.failMessage(500, "FAIL", jsonObjectResponse.getString("errorMessage"));
            }
            return CommonResult.success("SUCCESS");
        } catch (Exception e) {
            log.error("Kapitalbank 查询付款状态操作异常: " + e);
            return CommonResult.fail(500, "NetworkTimeout");
        }
    }
}
