package com.tomorrow.payments_hodgepodge.service;

import com.tomorrow.payments_hodgepodge.util.CommonResult;

/**
 * @author Tomorrow
 * @date 2020/11/17 20:07
 */
public interface KapitalbankService {
    /**
     * Kapitalbank 绑卡
     *
     * @param cardNumber   银行卡卡号
     * @param expYearMonth 月年份
     * @return
     */
    CommonResult kapitalbankTiedCard(String cardNumber, String expYearMonth);

    /**
     * Kapitalbank 创建交易
     *
     * @param amount 金额
     * @param token  绑卡后获取到的用户token
     * @return
     */
    CommonResult createKapitalbank(int amount, String token);

    /**
     * Kapitalbank 交易付款
     *
     * @param smsCode       短信验证码(在创建交易后就会收到短信)
     * @param transactionId 创建交易后返回的ID
     * @return
     */
    CommonResult payKapitalbank(String smsCode, String transactionId);

    /**
     * Kapitalbank 查询付款状态
     *
     * @param transactionId 创建交易后返回的ID
     * @return
     */
    CommonResult quyerKapitalbank(String transactionId);
}
