package com.tomorrow.payments_hodgepodge.service;

import com.tomorrow.payments_hodgepodge.util.CommonResult;

/**
 * 美国的一家支付
 *
 * @author Tomorrow
 * @date 2020/11/9 17:02
 */
public interface AuthorizeService {

    /**
     * authorize 支付
     *
     * @param orderNo 订单号
     * @return
     */
    CommonResult authorizePay(String orderNo);

    /**
     * authorize 退款
     *
     * @param amount  退款金额
     * @param transId authorize事物ID(支付成功后返回的参数)
     * @return
     */
    CommonResult authorizeRefund(String amount, String transId);

    /**
     * authorize 查询
     *
     * @param transId authorize事物ID(支付成功后返回的参数)
     * @return
     */
    CommonResult authorizeQuery(String transId);
}
