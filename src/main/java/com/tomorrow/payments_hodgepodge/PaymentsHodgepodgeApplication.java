package com.tomorrow.payments_hodgepodge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentsHodgepodgeApplication {

    public static void main(String[] args) {
        SpringApplication.run(PaymentsHodgepodgeApplication.class, args);
    }

}
