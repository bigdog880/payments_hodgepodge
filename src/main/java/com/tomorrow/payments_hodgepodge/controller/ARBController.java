package com.tomorrow.payments_hodgepodge.controller;

import com.tomorrow.payments_hodgepodge.service.ARBService;
import com.tomorrow.payments_hodgepodge.util.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Tomorrow
 * @date 2020/11/3 17:41
 */
@RestController
@RequestMapping("/arb")
public class ARBController {
    @Autowired
    private ARBService arbService;

    /**
     * 创建订单
     *
     * @param orderNo 订单号
     * @return
     */
    @RequestMapping(value = "/createOrder", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult createOrder(
            @RequestParam(value = "orderNo", required = true) String orderNo
    ) {
        return arbService.createOrder(orderNo);
    }

    /**
     * 回调地址
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/callback")
    @ResponseBody
    public CommonResult callback(
            HttpServletRequest request
    ) {
        return arbService.callback(request);
    }

    /**
     * ARB 退款
     *
     * @param amount        退款金额
     * @param refundOrderNo 退款ID支付成功后返回的数据
     * @return
     */
    @RequestMapping(value = "/arbRefund", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult arbRefund(
            @RequestParam(value = "amount", required = true) String amount,
            @RequestParam(value = "refundOrderNo", required = true) String refundOrderNo
    ) {
        return arbService.arbRefund(amount, refundOrderNo);
    }
}
