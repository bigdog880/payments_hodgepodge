package com.tomorrow.payments_hodgepodge.controller;

import com.tomorrow.payments_hodgepodge.service.KapitalbankService;
import com.tomorrow.payments_hodgepodge.util.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Tomorrow
 * @date 2020/11/17 20:07
 */
@RestController
@RequestMapping("/kapitalbank")
public class KapitalbankController {
    @Autowired
    private KapitalbankService kapitalbankService;

    /**
     * Kapitalbank 绑卡
     *
     * @param cardNumber   银行卡卡号
     * @param expYearMonth 月年份
     * @return
     */
    @RequestMapping(value = "/kapitalbankTiedCard", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult kapitalbankTiedCard(
            @RequestParam(value = "cardNumber", required = true) String cardNumber,
            @RequestParam(value = "expYearMonth", required = true) String expYearMonth
    ) {
        return kapitalbankService.kapitalbankTiedCard(cardNumber, expYearMonth);
    }

    /**
     * Kapitalbank 创建交易
     *
     * @param amount 金额
     * @param token  绑卡后获取到的用户token
     * @return
     */
    @RequestMapping(value = "/createKapitalbank", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult createKapitalbank(
            @RequestParam(value = "amount", required = true) Integer amount,
            @RequestParam(value = "token", required = true) String token
    ) {
        return kapitalbankService.createKapitalbank(amount, token);
    }


    /**
     * Kapitalbank 交易付款
     *
     * @param smsCode       短信验证码(在创建交易后就会收到短信)
     * @param transactionId 创建交易后返回的ID
     * @return
     */
    @RequestMapping(value = "/payKapitalbank", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult payKapitalbank(
            @RequestParam(value = "smsCode", required = true) String smsCode,
            @RequestParam(value = "transactionId", required = true) String transactionId
    ) {
        return kapitalbankService.payKapitalbank(smsCode, transactionId);
    }

    /**
     * Kapitalbank 查询付款状态
     *
     * @param transactionId 创建交易后返回的ID
     * @return
     */
    @RequestMapping(value = "/quyerKapitalbank", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult quyerKapitalbank(
            @RequestParam(value = "transactionId", required = true) String transactionId
    ) {
        return kapitalbankService.quyerKapitalbank(transactionId);
    }
}
