package com.tomorrow.payments_hodgepodge.controller;

import com.tomorrow.payments_hodgepodge.service.AuthorizeService;
import com.tomorrow.payments_hodgepodge.util.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Tomorrow
 * @date 2020/11/9 17:02
 */
@RestController
@RequestMapping("/authorize")
public class AuthorizeController {
    @Autowired
    private AuthorizeService authorizeService;

    /**
     * authorize 支付
     *
     * @param orderNo 订单号
     * @return
     */
    @RequestMapping(value = "/authorizePay", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult authorizePay(
            @RequestParam(value = "orderNo", required = true) String orderNo
    ) {
        return authorizeService.authorizePay(orderNo);
    }

    /**
     * authorize 退款
     *
     * @param amount  退款金额
     * @param transId authorize事物ID(支付成功后返回的参数)
     * @return
     */
    @RequestMapping(value = "/authorizeRefund", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult authorizeRefund(
            @RequestParam(value = "amount", required = true) String amount,
            @RequestParam(value = "transId", required = true) String transId
    ) {
        return authorizeService.authorizeRefund(amount, transId);
    }

    /**
     * authorize 查询
     *
     * @param transId authorize事物ID(支付成功后返回的参数)
     * @return
     */
    @RequestMapping(value = "/authorizeQuery", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult authorizeQuery(
            @RequestParam(value = "transId", required = true) String transId
    ) {
        return authorizeService.authorizeQuery(transId);
    }
}
