package com.tomorrow.payments_hodgepodge.controller;

import com.tomorrow.payments_hodgepodge.service.PayPalService;
import com.tomorrow.payments_hodgepodge.util.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Tomorrow
 * @date 2020/5/23 0:59
 */
@RestController
@RequestMapping("/paypal")
public class PayPalController {
    @Autowired
    private PayPalService payScoreService;

    /**
     * 创建订单
     *
     * @param amount 金额
     * @return
     */
    @RequestMapping(value = "/createOrder", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult createOrder(
            @RequestParam(value = "amount", required = false) String amount
    ) {
        return payScoreService.createOrder(amount);
    }

    /**
     * 捕获订单
     *
     * @param token 订单号
     * @return
     */
    @RequestMapping(value = "/captureOrder", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult captureOrder(
            @RequestParam(value = "token", required = true) String token
    ) {
        return payScoreService.captureOrder(token);
    }


    /**
     * 查询订单
     *
     * @param token 订单号
     * @return
     */
    @RequestMapping(value = "/queryOrder", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult refund(
            @RequestParam(value = "token", required = false) String token
    ) {
        return payScoreService.queryOrder(token);
    }

    /**
     * 申请退款
     *
     * @param token 订单号
     * @return
     */
    @RequestMapping(value = "/refundOrder", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult refundOrder(
            @RequestParam(value = "token", required = false) String token
    ) {
        return payScoreService.refundOrder(token);
    }

    /**
     * 支付成功回调通知
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/paypalSuccess", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult paypalSuccess(
            HttpServletRequest request
    ) {
        return payScoreService.paypalSuccess(request);
    }

    /**
     * 支付失败回调通知
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/paypalFail", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult paypalFail(
            HttpServletRequest request
    ) {
        return payScoreService.paypalFail(request);
    }
}
