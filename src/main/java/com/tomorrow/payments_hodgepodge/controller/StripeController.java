package com.tomorrow.payments_hodgepodge.controller;

import com.tomorrow.payments_hodgepodge.service.StripeService;
import com.tomorrow.payments_hodgepodge.util.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Tomorrow
 * @date 2020/11/13 14:57
 */
@RestController
@RequestMapping("/stripe")
public class StripeController {
    @Autowired
    private StripeService stripeService;

    /**
     * 创建 SetupIntent
     *
     * @return
     */
    @RequestMapping(value = "/createSetupIntent", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult createSetupIntent() {
        return stripeService.createSetupIntent();
    }

    /**
     * 获取银行卡数据
     *
     * @return
     */
    @RequestMapping(value = "/getBankCardInformation", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult getBankCardInformation(
            @RequestParam(value = "customerId", required = true) String customerId
    ) {
        return stripeService.getBankCardInformation(customerId);
    }

    /**
     * 支付
     *
     * @return
     */
    @RequestMapping(value = "/payOrder", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult payOrder(
            @RequestParam(value = "customerId", required = true) String customerId,
            @RequestParam(value = "paymentMethodId", required = true) String paymentMethodId
    ) {
        return stripeService.payOrder(paymentMethodId, customerId);
    }



    /**
     * 授权
     *
     * @param orderNo 订单号
     * @return
     */
    @RequestMapping(value = "/stripeAuthorize", method = RequestMethod.GET)
    @ResponseBody
    public CommonResult stripeAuthorize(
            @RequestParam(value = "orderNo", required = true) String orderNo
    ) {
        return stripeService.stripeAuthorize(orderNo);
    }

    /**
     * 捕获
     *
     * @param token
     * @return
     */
    @RequestMapping(value = "/stripeCapture")
    @ResponseBody
    public CommonResult stripeCapture(
            @RequestParam(value = "token", required = true) String token
    ) {
        return stripeService.stripeCapture(token);
    }
}
